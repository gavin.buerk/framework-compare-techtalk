import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { AppComponent } from './app.component';
import { metaReducers } from './app.meta-reducers';
import { FruitBasketModule } from './fruit-basket/fruit-basket.module';
import { HomePageComponent } from './routing/home-page/home-page.component';
import { RoutingModule } from './routing/routing.module';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
  ],
  imports: [
    BrowserModule,
    EffectsModule.forRoot([]),
    FruitBasketModule,
    HttpClientModule,
    RoutingModule,
    StoreModule.forRoot({}, {metaReducers}),
  ],
  bootstrap: [
    AppComponent,
  ],
})
export class AppModule {}

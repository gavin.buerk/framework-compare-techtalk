import { Chart } from 'chart.js';

import * as colors from '../colors/colors';

Chart.defaults.global.animation.duration = 750;
Chart.defaults.global.defaultFontColor = colors.black;
Chart.defaults.global.defaultFontFamily = `'Helvetica', sans-serif`;
Chart.defaults.global.defaultFontSize = 14;

export { Chart };

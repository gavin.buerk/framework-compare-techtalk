import { isDevMode } from '@angular/core';
import { Action, ActionReducer } from '@ngrx/store';

export const metaReducers = [];

if (isDevMode()) {
  metaReducers.push(debug);
}

function debug(reducer: ActionReducer<any>): ActionReducer<any> {
  return (state, action) => {
    if (isAppAction(action)) {
      logState(state);
      logAction(action);
    }

    return reducer(state, action);
  };
}

function isAppAction(action: Action): boolean {
  return !action.type.startsWith('@ngrx');
}

function logState(state: any): void {
  console.log('Current state: ' + JSON.stringify(state, undefined, 2));
}

function logAction(action: Action): void {
  console.log(`Dispatched action with type "${action.type}"`);
}

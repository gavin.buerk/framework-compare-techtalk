import { createFeatureSelector, createSelector } from '@ngrx/store';

import { FruitBasket } from './fruit-basket';
import { FruitBasketState } from './fruit-basket.state';

const featureSelector = createFeatureSelector<FruitBasketState>('fruitBasket');

export const fruitBasketOptionsSelector = createSelector(featureSelector, featureState => {
  if (featureState) {
    return featureState.fruitBasketOptions;
  }
});

export const selectedFruitBasketSelector = createSelector(featureSelector, featureState => {
  if (featureState) {
    return featureState.selectedFruitBasket;
  }
});

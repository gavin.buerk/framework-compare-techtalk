import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { FruitBasket } from '../fruit-basket';
import { selectFruitBasket } from '../fruit-basket.actions';
import { fruitBasketOptionsSelector, selectedFruitBasketSelector } from '../fruit-basket.selectors';
import { FruitBasketState } from '../fruit-basket.state';

@Component({
  selector: 'fruit-basket-spreadsheet',
  templateUrl: './spreadsheet.component.html',
  styleUrls: ['./spreadsheet.component.scss'],
})
export class SpreadsheetComponent implements OnInit {
  public fruitBasketOptions: Observable<FruitBasket[]>;
  public selectedFruitBasket: Observable<FruitBasket>;

  constructor(private store: Store<FruitBasketState>) {}

  ngOnInit() {
    this.fruitBasketOptions = this.store.select(fruitBasketOptionsSelector);
    this.selectedFruitBasket = this.store.select(selectedFruitBasketSelector);
  }

  public selectFruitBasket(fruitBasket: FruitBasket): void {
    this.store.dispatch(selectFruitBasket({fruitBasket}));
  }
}

import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';

import { FruitBasket } from '../fruit-basket';
import { selectedFruitBasketSelector } from '../fruit-basket.selectors';
import { FruitBasketState } from '../fruit-basket.state';
import { Chart } from '../../shared/chart/chart';
import * as colors from '../../shared/colors/colors';

@Component({
  selector: 'fruit-basket-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss'],
})
export class PieChartComponent implements OnInit, OnDestroy {
  @ViewChild('canvas') canvas: ElementRef;

  private chart: Chart;
  private selectedFruitBasket: FruitBasket;
  private selectedFruitBasketSubscription: Subscription;

  constructor(private store: Store<FruitBasketState>) {}

  ngOnInit() {
    this.selectedFruitBasketSubscription = this.subscribeToSelectedFruitBasket();
  }

  ngOnDestroy() {
    this.selectedFruitBasketSubscription.unsubscribe();
  }

  private initChart(): void {
    this.chart =  new Chart(this.canvas.nativeElement, {
      type: 'pie',
      data: {
        datasets: [{
          data: [
            this.selectedFruitBasket.nApples,
            this.selectedFruitBasket.nOranges,
          ],
          backgroundColor: [colors.red, colors.orange],
        }],
        labels: ['Apples', 'Oranges'],
      },
    });
  }

  private subscribeToSelectedFruitBasket(): Subscription {
    return this.store.select(selectedFruitBasketSelector).subscribe(selectedFruitBasket => {
      this.selectedFruitBasket = selectedFruitBasket;

      if (!this.selectedFruitBasket) {
        return;
      }

      if (!this.chart) {
        this.initChart();
      } else {
        this.updateChart();
      }
    });
  }

  private updateChart(): void {
    this.chart.data.datasets[0].data = [
      this.selectedFruitBasket.nApples,
      this.selectedFruitBasket.nOranges,
    ];

    this.chart.update();
  }
}

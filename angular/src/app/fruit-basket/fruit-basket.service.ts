import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { FruitBasket } from './fruit-basket';

enum Endpoint {
  GetFruitBasketOptions = 'http://localhost:3000/api/fruit-basket-options',
}

@Injectable({
  providedIn: 'root',
})
export class FruitBasketService {
  constructor(private httpClient: HttpClient) {}

  public getFruitBasketOptions(): Observable<FruitBasket[]> {
    return this.httpClient.get<FruitBasket[]>(Endpoint.GetFruitBasketOptions);
  }
}

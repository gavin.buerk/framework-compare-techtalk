import { FruitBasket } from './fruit-basket';

export interface FruitBasketState {
  fruitBasketOptions: FruitBasket[];
  selectedFruitBasket: FruitBasket;
}

import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';

import { FruitBasket } from '../fruit-basket';
import { selectedFruitBasketSelector } from '../fruit-basket.selectors';
import { FruitBasketState } from '../fruit-basket.state';
import { Chart } from '../../shared/chart/chart';
import * as colors from '../../shared/colors/colors';

@Component({
  selector: 'fruit-basket-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss'],
})
export class BarChartComponent implements OnInit, OnDestroy {
  @ViewChild('canvas') canvas: ElementRef;

  private chart: Chart;
  private selectedFruitBasket: FruitBasket;
  private selectedFruitBasketSubscription: Subscription;

  constructor(private store: Store<FruitBasketState>) {}

  ngOnInit() {
    this.selectedFruitBasketSubscription = this.subscribeToSelectedFruitBasket();
  }

  ngOnDestroy() {
    this.selectedFruitBasketSubscription.unsubscribe();
  }

  private initChart(): void {
    this.chart =  new Chart(this.canvas.nativeElement, {
      type: 'bar',
      data: {
        datasets: [{
          data: [
            this.selectedFruitBasket.nApples,
            this.selectedFruitBasket.nOranges,
          ],
          backgroundColor: [colors.red, colors.orange],
        }],
        labels: ['Apples', 'Oranges'],
      },
      options: {
        legend: {
          display: false,
        },
        scales: {
          xAxes: [{
            barPercentage: 0.75,
            gridLines: {
              color: colors.lightGray,
            },
          }],
          yAxes: [{
            gridLines: {
              color: colors.lightGray,
              zeroLineColor: colors.lightGray,
            },
            ticks: {
              beginAtZero: true,
              callback: () => '',
              suggestedMax: this.selectedFruitBasket.nApples + this.selectedFruitBasket.nOranges,
              suggestedMin: 0,
            }
          }],
        },
      },
    });
  }

  private subscribeToSelectedFruitBasket(): Subscription {
    return this.store.select(selectedFruitBasketSelector).subscribe(selectedFruitBasket => {
      this.selectedFruitBasket = selectedFruitBasket;

      if (!this.selectedFruitBasket) {
        return;
      }

      if (!this.chart) {
        this.initChart();
      } else {
        this.updateChart();
      }
    });
  }

  private updateChart(): void {
    this.chart.data.datasets[0].data = [
      this.selectedFruitBasket.nApples,
      this.selectedFruitBasket.nOranges,
    ];

    this.chart.options.scales.yAxes[0].ticks.suggestedMax = this.selectedFruitBasket.nApples + this.selectedFruitBasket.nOranges;

    this.chart.update();
  }
}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { Store, StoreModule } from '@ngrx/store';

import { BarChartComponent } from './bar-chart/bar-chart.component';
import { getFruitBasketOptions, selectFruitBasket } from './fruit-basket.actions';
import { FruitBasketEffects } from './fruit-basket.effects';
import { fruitBasketOptionsReducer, selectedFruitBasketReducer } from './fruit-basket.reducers';
import { fruitBasketOptionsSelector } from './fruit-basket.selectors';
import { FruitBasketState } from './fruit-basket.state';
import { PieChartComponent } from './pie-chart/pie-chart.component';
import { SpreadsheetComponent } from './spreadsheet/spreadsheet.component';

@NgModule({
  declarations: [
    BarChartComponent,
    PieChartComponent,
    SpreadsheetComponent,
  ],
  imports: [
    CommonModule,
    EffectsModule.forFeature([FruitBasketEffects]),
    StoreModule.forFeature('fruitBasket', {
      fruitBasketOptions: fruitBasketOptionsReducer,
      selectedFruitBasket: selectedFruitBasketReducer,
    }),
  ],
  exports: [
    BarChartComponent,
    PieChartComponent,
    SpreadsheetComponent,
  ],
})
export class FruitBasketModule {
  constructor(private store: Store<FruitBasketState>) {
    this.initState();
  }

  private initState(): void {
    const fruitBasketOptionsSubscription = this.store.select(fruitBasketOptionsSelector).subscribe(fruitBasketOptions => {
      if (fruitBasketOptions && fruitBasketOptions.length > 0) {
        const initIndex = Math.min(2, fruitBasketOptions.length - 1);
        this.store.dispatch(selectFruitBasket({fruitBasket: fruitBasketOptions[initIndex]}));
        fruitBasketOptionsSubscription.unsubscribe();
      }
    });

    this.store.dispatch(getFruitBasketOptions());
  }
}

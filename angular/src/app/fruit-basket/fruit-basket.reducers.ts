import { createReducer, on } from '@ngrx/store';

import { getFruitBasketOptionsFailure, getFruitBasketOptionsSuccess, selectFruitBasket } from './fruit-basket.actions';

export const fruitBasketOptionsReducer = createReducer(
  undefined,
  on(getFruitBasketOptionsFailure, state => state),
  on(getFruitBasketOptionsSuccess, (state, {fruitBasketOptions}) => fruitBasketOptions)
);

export const selectedFruitBasketReducer = createReducer(
  undefined,
  on(selectFruitBasket, (state, {fruitBasket}) => fruitBasket)
);

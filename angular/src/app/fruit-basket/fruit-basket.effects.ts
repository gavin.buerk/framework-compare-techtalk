import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';

import { getFruitBasketOptions, getFruitBasketOptionsFailure, getFruitBasketOptionsSuccess } from './fruit-basket.actions';
import { FruitBasketService } from './fruit-basket.service';

@Injectable()
export class FruitBasketEffects {
  public getFruitBasketOptions = createEffect(() => this.actions.pipe(
    ofType(getFruitBasketOptions),
    mergeMap(() => this.fruitBasketService.getFruitBasketOptions().pipe(
      map(fruitBasketOptions => getFruitBasketOptionsSuccess({fruitBasketOptions})),
      catchError(error => {
        console.error(error.message);
        return of(getFruitBasketOptionsFailure());
      })
    ))
  ));

  constructor(private actions: Actions, private fruitBasketService: FruitBasketService) {}
}

import { createAction, props } from '@ngrx/store';

import { FruitBasket } from './fruit-basket';

export const getFruitBasketOptions = createAction('getFruitBasketOptions');
export const getFruitBasketOptionsFailure = createAction('getFruitBasketOptionsFailure');
export const getFruitBasketOptionsSuccess = createAction('getFruitBasketOptionsSuccess', props<{fruitBasketOptions: FruitBasket[]}>());
export const selectFruitBasket = createAction('selectFruitBasket', props<{fruitBasket: FruitBasket}>());

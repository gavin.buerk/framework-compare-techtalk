export interface FruitBasket {
  nApples: number;
  nOranges: number;
}

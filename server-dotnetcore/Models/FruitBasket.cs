namespace FruitBasketApi.Models
{
    public class FruitBasket
    {
        public long Id { get; set; }
        public int nApples { get; set; }
        public int nOranges { get; set; }
    }
}
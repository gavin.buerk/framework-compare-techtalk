using Microsoft.EntityFrameworkCore;

namespace FruitBasketApi.Models
{
    public class FruitBasketContext : DbContext
    {
        public FruitBasketContext(DbContextOptions<FruitBasketContext> options)
            : base(options)
        {
        }

        public DbSet<FruitBasket> FruitBaskets { get; set; }
    }
}
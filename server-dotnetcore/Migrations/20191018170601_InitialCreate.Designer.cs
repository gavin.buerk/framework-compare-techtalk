﻿// <auto-generated />
using FruitBasketApi.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace FruitBasketApi.Migrations
{
    [DbContext(typeof(FruitBasketContext))]
    [Migration("20191018170601_InitialCreate")]
    partial class InitialCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.0.0");

            modelBuilder.Entity("FruitBasketApi.Models.FruitBasket", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<int>("nApples")
                        .HasColumnType("INTEGER");

                    b.Property<int>("nOranges")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.ToTable("FruitBaskets");
                });
#pragma warning restore 612, 618
        }
    }
}

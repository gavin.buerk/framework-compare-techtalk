using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FruitBasketApi.Models;

namespace FruitBasketApi.Controllers
{
    [Route("api/fruit-basket-options")]
    [ApiController]
    public class FruitBasketController : ControllerBase
    {
        private readonly FruitBasketContext _context;

        public FruitBasketController(FruitBasketContext context)
        {
            _context = context;
        }

        // GET: api/FruitBasket
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FruitBasket>>> GetFruitBaskets()
        {
            return await _context.FruitBaskets.ToListAsync();
        }

        // GET: api/FruitBasket/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FruitBasket>> GetFruitBasket(long id)
        {
            var fruitBasket = await _context.FruitBaskets.FindAsync(id);

            if (fruitBasket == null)
            {
                return NotFound();
            }

            return fruitBasket;
        }

        // PUT: api/FruitBasket/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFruitBasket(long id, FruitBasket fruitBasket)
        {
            if (id != fruitBasket.Id)
            {
                return BadRequest();
            }

            _context.Entry(fruitBasket).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FruitBasketExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/FruitBasket
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<FruitBasket>> PostFruitBasket(FruitBasket fruitBasket)
        {
            _context.FruitBaskets.Add(fruitBasket);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFruitBasket", new { id = fruitBasket.Id }, fruitBasket);
        }

        // DELETE: api/FruitBasket/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<FruitBasket>> DeleteFruitBasket(long id)
        {
            var fruitBasket = await _context.FruitBaskets.FindAsync(id);
            if (fruitBasket == null)
            {
                return NotFound();
            }

            _context.FruitBaskets.Remove(fruitBasket);
            await _context.SaveChangesAsync();

            return fruitBasket;
        }

        private bool FruitBasketExists(long id)
        {
            return _context.FruitBaskets.Any(e => e.Id == id);
        }
    }
}

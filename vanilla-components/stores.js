"use strict"
import WritableStore from './WritableStore.js'

export const fruitBasketOptionsStore = new function FruitBasketOptionsStore() {
    const writableStore = new WritableStore()
    async function init() {
        const response = await fetch('http://localhost:3000/api/fruit-basket-options')
        const fbos = await response.json()
        writableStore.updateSubscribers(fbos)
        const initIndex = Math.min(2, fbos.length - 1);
        selectedFruitBasketStore.selectFruitBasket(fbos[initIndex]);
    }
    
    init()
    return {
        subscribe: writableStore.subscribe,
    }
}

export const selectedFruitBasketStore = new function SelectedFruitBasketStore() {
    const writableStore = new WritableStore()
    return {
        subscribe: writableStore.subscribe,
        selectFruitBasket(fruitBasket) {
            writableStore.updateSubscribers(fruitBasket)
        }
    }
}
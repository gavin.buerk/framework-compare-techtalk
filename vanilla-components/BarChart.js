"use strict";

import Chart from './chart.js'
import colors from './colors.js'
import {selectedFruitBasketStore} from './stores.js'

export default function BarChart() {
    let chart
    let querySelector = `[component="BarChart"]`

    document.querySelector( querySelector ).innerHTML = `
        <canvas class="chart" width="250" height="250" ></canvas>
    `

    function initChart(selectedFruitBasket) {
        chart = new Chart(document.querySelector(querySelector + " .chart"), {
            type: 'bar',
            data: {
                datasets: [{
                    data: [
                        selectedFruitBasket.nApples,
                        selectedFruitBasket.nOranges
                    ],
                    backgroundColor: [colors.red, colors.orange],
                }],
                labels: ['Apples', 'Oranges'],
            },
            options: {
                legend: {
                    display: false,
                },
                scales: {
                    xAxes: [{
                        barPercentage: 0.75,
                        gridLines: {
                            color: colors.lightGray,
                        },
                    }],
                    yAxes: [{
                        gridLines: {
                            color: colors.lightGray,
                            zeroLineColor: colors.lightGray,
                        },
                        ticks: {
                            beginAtZero: true,
                            callback: () => '',
                            suggestedMax: selectedFruitBasket.nApples + selectedFruitBasket.nOranges,
                            suggestedMin: 0,
                        }
                    }],
                },
            },
        });
    }

    function updateChart(selectedFruitBasket) {
        chart.data.datasets[0].data = [
            selectedFruitBasket.nApples,
            selectedFruitBasket.nOranges,
        ];

        chart.update();
    }

    function renderChart(selectedFruitBasket) {
        if (!selectedFruitBasket) return;
        if (!chart) {
            initChart(selectedFruitBasket)
        } else {
            updateChart(selectedFruitBasket)
        }
    }

    selectedFruitBasketStore.subscribe(selectedFruitBasket => {
        renderChart(selectedFruitBasket)
    })
}


"use strict";

export default {
    black: '#212121',
    gray: '#9e9e9e',
    lightGray: '#bdbdbd',
    lighterGray: '#e0e0e0',
    orange: '#ffb74d',
    lightOrange: '#ffcc80',
    lighterOrange: '#ffe0b2',
    red: '#e57373',
    lightRed: '#ef9a9a',
    lighterRed: '#ffcdd2',
    white: '#ffffff',
    offWhite: '#fafafa',
}
"use strict";

import Spreadsheet from './Spreadsheet.js'
import BarChart from './BarChart.js'
import PieChart from './PieChart.js'
import {selectedFruitBasketStore, fruitBasketOptionsStore} from './stores.js'

export default function Home() {
    document.querySelector( `[component="Home"]` ).innerHTML = `
        <div component="Spreadsheet" class="spreadsheet" ></div>
        <div component="BarChart" class="chartwrapper" ></div>
        <div component="PieChart" class="chartwrapper" ></div>
    `

    new Spreadsheet()
    new BarChart()
    new PieChart()

    selectedFruitBasketStore.subscribe(selectedFruitBasket => {
        console.log(`Selected fruit basket updated: ${JSON.stringify(selectedFruitBasket, null, 2)}`)
    })
    
    fruitBasketOptionsStore.subscribe(fruitBasketOptions => {
        console.log(`Fruit Basket Options updated: ${JSON.stringify(fruitBasketOptions, null, 2)}`)
    })
}
# Vanilla JS Implementation

## Notes

This was an interesting process.  I was forced to think through how to structure my solution rationally without any framework dictating the best practice.  In this solution:

* I did end up breaking the app into "components" (used very loosely, as they're really just shells or traditional components). This was not _necessary_, as I describe below in the `Process` section, but improved the structure of my code immensely.
* I stuck to my original goal of ZERO dependencies (with the exception of Chart.js), including build time tools, etc.  None.  Only vanilla ES6 JS
* At one point, I unintentionally started down a path that looked an _awful_ lot like React.  I backed off it when I realized what was happening, but I left a remnant of the work under `SpreadsheetReact.js`.  That file is not currently being referenced anywhere in the project.
    * In the file, the reactivity model is based on an internal state and any change that causes a reaction simply re-renders the whole component.
* With the exception of Routing (missing) and SASS (excluded by definition), this implementation is very analigous to the rest of the implementations in this project.
* This is, by a long shot, the "slimmest" implementation.  Even though it's not minified or gzipped or anything (again, by design - vanilla JS only), and compared to others that are (production ready builds), here are the numbers of bytes delivered to the browser
    | Impl | Size |
    | --- | --- |
    | Vanilla | About 56k |
    | Svelte | About 217k |
    | React | About 373k |
    | Vue | About 574k |

#### My overall impression is that this works ok, and it's cool to know it's possible, but I would never write any modern software this way.  There are way too many nice features that modern frameworks give you for free.  Most will even let you incrementally adopt, which makes it really easy to pull them in to vanilla setups (no dev tooling) and still gain productivity right off the bat.  Vue, in particular, is awesome at that.  



## The process

* I started with index.html, index.css, and index.js and built the whole app in 3 files.  I could quickly tell this wouldn't work for a project much bigger than mine.
    * There was no namespacing, virtually everything was global.  State was stored in global variables
    * When an event occurred (like selecting a new fruit basket), the handler was coupled to all of the dependencies (re-decide which fruit basket is active, update that dom, update the bar chart, update the spreadsheet, log to the console).  This worked, but will lead to confusing update logic as the app continues to grow.
* I decided I wanted to split the concerns associated with the event (selectFruitBasket) callback.  So, I set up a pub sub mechanism similar to most of the other frameworks.  This worked great, much cleaner.
* At this point though, I was starting to get lost in index.js.  There was just too much code.  So, I decided to pull "store" related functionality into a separate js file and add it to the index.html.  This worked great.  So, I started looking for other places I could tease functionality out to its own file.
* I pulled the JS related to rendering the pie chart into its own file.  At this point, it was not a "component" by any sense.  It was just an external module that I could instantiate and use.  It worked great.
* Next, I pulled out the Bar chart JS.  Easy.
* Next, I pulled out all the callbacks related to updating the dom in the Spreadsheet.  Here, it started to feel a little strange because those callbacks have dependencies on other things, like the stores.  Also, what to do about that pesky onclick trigger from the Spreadsheet section of the dom?  
* This is where I started pulling out markup from index.html.  I started with the piechart canvas, to prove my concept.  It worked great.  So, I moved to the others (BarChart and Spreadsheet)
* Then, I was able to pull everything else related to servicing those sections of the dom (I may start calling them components now) into their own JS files.  
* At this point, index.js started looking pretty small.  All I was doing was instantiating Spreadsheet, PieChart, and BarChart, and console logging change events!  Awesome.
* But, why instantiate 3 components, when you can instantiate 1 and let that one instantiate the others for you?  So, I pushed that logic down further again.  This is where I introduced Home.  
    * It's important to note that Home is a concept from the other Fruit Basket apps because they use client side routing.  The Home component is what is tied to the "/home" route.  It's not just gratuitous.  
* Then, I created the App component and removed all the markup from index.html.  So simple. Now, index.js just instantiates the App, the rest is bootstrapped from there.
* Everything is properly namespaced using JS functions.  At one point, that stopped even mattering because the API of each component dropped to nothing, which is super awesome because when I started that was _definitely_ not the case.
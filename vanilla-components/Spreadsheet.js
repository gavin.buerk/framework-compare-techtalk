"use strict";
import {selectedFruitBasketStore, fruitBasketOptionsStore} from './stores.js'

export default function Spreadsheet() {
    const querySelector = `[component="Spreadsheet"]`;

    document.querySelector(querySelector).innerHTML = `
        <table>
            <thead>
                <tr>
                    <th class="apples-header">Apples</th>
                    <th class="oranges-header">Oranges</th>
                </tr>
            </thead>

            <tbody id="spreadsheetTable">
            </tbody>
        </table>
    `

    function updateFruitBasketOptionsDom(fbos) {
        const spreadsheetTable = document.getElementById('spreadsheetTable')
        spreadsheetTable.innerHTML = fbos.map(fbo => `
            <tr data-n-apples="${fbo.nApples}" data-n-oranges="${fbo.nOranges}">
                <td class="apples-data">${fbo.nApples}</td>
                <td class="oranges-data">${fbo.nOranges}</td>
            </tr>
        `).join('')
        spreadsheetTable.querySelectorAll('tr').forEach(tr => tr.addEventListener('click', selectFruitBasket))
    }

    function selectFruitBasket(event) {
        const {nApples, nOranges} = event.currentTarget.dataset
        const fruitBasket = { nApples: Number(nApples), nOranges: Number(nOranges) }
        selectedFruitBasketStore.selectFruitBasket(fruitBasket)
    }
    
    function updateSelectedFruitBasketDom(selectedFruitBasket) {
        document.querySelectorAll(`${querySelector} [data-n-apples]`).forEach(r => r.classList.remove('selected-fruit-basket'))
        const selectedFruitBasketRow = document.querySelector(`${querySelector} [data-n-apples='${selectedFruitBasket.nApples}'],[data-n-oranges='${selectedFruitBasket.nOranges}']`)
        selectedFruitBasketRow.classList.add('selected-fruit-basket')
    }

    selectedFruitBasketStore.subscribe(selectedFruitBasket => {
        updateSelectedFruitBasketDom(selectedFruitBasket)
    })

    fruitBasketOptionsStore.subscribe(fruitBasketOptions => {
        updateFruitBasketOptionsDom(fruitBasketOptions)
    })
    
}
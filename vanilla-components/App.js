"use strict";
import Home from './Home.js'

export default function App() {
    document.querySelector(`[component="App"]`).innerHTML = `
        <div component="Home" class="wrapper"></div>
    `
    new Home()
}
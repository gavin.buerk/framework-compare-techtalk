export default function WritableStore() {
    let subscribers = []
    function subscribe(subscriptionCallback) {
        subscribers.push(subscriptionCallback)
        return () => {
            subscribers = subscribers.filter(s => s !== subscriptionCallback)
        }
    }
    function updateSubscribers(newValue) {
        subscribers.forEach(s => s(newValue))
    }
    return {
        subscribe,
        updateSubscribers
    };
}
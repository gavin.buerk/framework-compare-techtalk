"use strict";
import {selectedFruitBasketStore, fruitBasketOptionsStore} from './stores.js'

export default function Spreadsheet () {
    const state = {
        fbos: [],
        selectFruitBasket: {}
    }

    function render({fbos, selectedFruitBasket}) {
        const root = document.querySelector( '[component="Spreadsheet"]' )
        root.innerHTML = `
            <table>
                <thead>
                    <tr>
                        <th class="apples-header">Apples</th>
                        <th class="oranges-header">Oranges</th>
                    </tr>
                </thead>

                <tbody id="spreadsheetTable">
                ${fbos.map(fbo => `
                    <tr class="${selectedFruitBasket && selectedFruitBasket.nApples === fbo.nApples && selectedFruitBasket.nOranges === fbo.nOranges ? 'selected-fruit-basket' : ''}" 
                                data-n-apples="${fbo.nApples}" 
                                data-n-oranges="${fbo.nOranges}" >
                        <td class="apples-data">${fbo.nApples}</td>
                        <td class="oranges-data">${fbo.nOranges}</td>
                    </tr>
                `).join('')}
                </tbody>
            </table>
        `
        root.querySelectorAll('tr').forEach(tr => tr.addEventListener('click', selectFruitBasket))
    }
    render(state)

    function updateFruitBasketOptionsDom(fbos) {
        state.fbos = fbos;
        render(state)
    }
    
    function updateSelectedFruitBasketOptionDom(selectedFruitBasket) {
        state.selectedFruitBasket = selectedFruitBasket
        render(state)
    }

    selectedFruitBasketStore.subscribe(selectedFruitBasket => {
        updateSelectedFruitBasketOptionDom(selectedFruitBasket)
    })

    fruitBasketOptionsStore.subscribe(fruitBasketOptions => {
        updateFruitBasketOptionsDom(fruitBasketOptions)
    })
    function selectFruitBasket(event) {
        const {nApples, nOranges} = event.currentTarget.dataset
        const fruitBasket = { nApples: Number(nApples), nOranges: Number(nOranges) }
        selectedFruitBasketStore.selectFruitBasket(fruitBasket)
    }
}

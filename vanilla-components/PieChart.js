"use strict";
import Chart from './chart.js'
import colors from './colors.js'
import {selectedFruitBasketStore} from './stores.js'

export default function PieChart() {
    let chart
    let querySelector = `[component="PieChart"]`

    document.querySelector( querySelector ).innerHTML = `
        <canvas class="chart" width="250" height="250" ></canvas>
    `

    function initChart(selectedFruitBasket) {
        chart = new Chart(document.querySelector(querySelector + ' .chart'), {
            type: "pie",
            data: {
                datasets: [
                    {
                        data: [
                            selectedFruitBasket.nApples,
                            selectedFruitBasket.nOranges
                        ],
                        backgroundColor: [colors.red, colors.orange]
                    }
                ],
                labels: ["Apples", "Oranges"]
            }
        });
    }

    function updateChart(selectedFruitBasket) {
        chart.data.datasets[0].data = [
            selectedFruitBasket.nApples,
            selectedFruitBasket.nOranges,
        ];
    
        chart.update();
    }

    function renderChart(selectedFruitBasket) {
        if (!selectedFruitBasket) return
        if (!chart) {
            initChart(selectedFruitBasket)
        } else {
            updateChart(selectedFruitBasket)
        }
    }

    selectedFruitBasketStore.subscribe(selectedFruitBasket => {
        renderChart(selectedFruitBasket)
    })
}
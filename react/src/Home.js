import React, { useState } from 'react';
import FruitBasketBarChart from './components/FruitBasketBarChart'
import FruitBasketPieChart from './components/FruitBasketPieChart'
import FruitBasketSpreadsheet from './components/FruitBasketSpreadsheet'
import './Home.scss';
import { useSelector } from 'react-redux';

export default () => {
    const fruitBasketOptions = useSelector(state => state.fruitBasketOptions);
    const selectedFruitBasket = useSelector(state => state.selectedFruitBasket);

    const [internalSelectedFruitBasket, setInternalSelectedFruitBasket] = useState(selectedFruitBasket);
    const [internalFruitBasketOptions, setInternalFruitBasketOptions] = useState(fruitBasketOptions);

    if (selectedFruitBasket !== internalSelectedFruitBasket) {
        setInternalSelectedFruitBasket(selectedFruitBasket)
        console.log(`Selected Fruit Basket updated: ${JSON.stringify(selectedFruitBasket, null, 2)}`)
    }
    if (fruitBasketOptions !== internalFruitBasketOptions) {
        setInternalFruitBasketOptions(fruitBasketOptions)
        console.log(`Fruit Basket Options updated: ${JSON.stringify(fruitBasketOptions, null, 2)}`)
    }
    return (
        <div className="home">
            <FruitBasketSpreadsheet/>
            <FruitBasketBarChart/>
            <FruitBasketPieChart/>
        </div>
    )
}
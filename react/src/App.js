import React from "react";
import "./App.scss";
import Home from "./Home";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";

export default () => (
  <Router>
    <div className="App">
      <Route path="/" exact render={() => <Redirect to="/home" />} />
      <Route path="/home" component={Home} />
    </div>
  </Router>
);

import React, { useEffect, useRef, useState } from 'react';
import { Chart } from '../shared/chart';
import * as colors from '../shared/colors.js';
import { useSelector } from 'react-redux';
import './FruitBasketPieChart.scss';

export default () => {
    const canvasElement = useRef(null);
    const [chart, setChart] = useState()

    const selectedFruitBasket = useSelector(state => state.selectedFruitBasket)

    useEffect(() => {
        subscriptionUpdated()
    });

    function initChart() {
        setChart(new Chart(canvasElement.current, {
            type: "pie",
            data: {
                datasets: [
                    {
                        data: [
                            selectedFruitBasket.nApples,
                            selectedFruitBasket.nOranges
                        ],
                        backgroundColor: [colors.red, colors.orange]
                    }
                ],
                labels: ["Apples", "Oranges"]
            }
        }));
    }
    
    function updateChart() {
        chart.data.datasets[0].data = [
            selectedFruitBasket.nApples,
            selectedFruitBasket.nOranges,
        ];
    
        chart.update();
    }
    
    function subscriptionUpdated() {
        if (!canvasElement) return;
        if (!chart) {
            initChart(selectedFruitBasket)
        } else {
            updateChart()
        }
    }

    return (
        <div className="piechart">
            <canvas ref={canvasElement} width="250" height="250"/>
        </div>
    )
}
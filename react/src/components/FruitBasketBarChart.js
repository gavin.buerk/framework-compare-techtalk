import React, { useEffect, useRef, useState } from 'react';
import { Chart } from '../shared/chart';
import * as colors from '../shared/colors.js';
import { useSelector } from 'react-redux';
import './FruitBasketBarChart.scss';

export default () => {
    const canvasElement = useRef(null);
    const [chart, setChart] = useState()

    const selectedFruitBasket = useSelector(state => state.selectedFruitBasket)

    useEffect(() => {
        subscriptionUpdated()
    });

    function initChart() {
        setChart(new Chart(canvasElement.current, {
            type: 'bar',
            data: {
              datasets: [{
                data: [
                    selectedFruitBasket.nApples,
                    selectedFruitBasket.nOranges
                ],
                backgroundColor: [colors.red, colors.orange],
              }],
              labels: ['Apples', 'Oranges'],
            },
            options: {
              legend: {
                display: false,
              },
              scales: {
                xAxes: [{
                  barPercentage: 0.75,
                  gridLines: {
                    color: colors.lightGray,
                  },
                }],
                yAxes: [{
                  gridLines: {
                    color: colors.lightGray,
                    zeroLineColor: colors.lightGray,
                  },
                  ticks: {
                    beginAtZero: true,
                    callback: () => '',
                    suggestedMax: selectedFruitBasket.nApples + selectedFruitBasket.nOranges,
                    suggestedMin: 0,
                  }
                }],
              },
            },
          })
        );
    }

    function updateChart() {
        chart.data.datasets[0].data = [
            selectedFruitBasket.nApples,
            selectedFruitBasket.nOranges,
        ];
    
        chart.options.scales.yAxes[0].ticks.suggestedMax = selectedFruitBasket.nApples + selectedFruitBasket.nOranges;

        chart.update();
    }
    
    function subscriptionUpdated() {
        if (!canvasElement) return;
        if (!chart) {
            initChart(selectedFruitBasket)
        } else {
            updateChart()
        }
    }

    return (
        <div className="barchart">
            <canvas ref={canvasElement} width="250" height="250"/>
        </div>
    )  
} 
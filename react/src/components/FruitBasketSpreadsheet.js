import React from 'react';
import './FruitBasketSpreadsheet.scss';
import { useSelector, useDispatch } from 'react-redux';

export default () => {

    const dispatch = useDispatch();
    const fruitBasketOptions = useSelector(state => state.fruitBasketOptions);
    const selectedFruitBasket = useSelector(state => state.selectedFruitBasket);

    function selectFruitBasket(fruitBasket) {
        dispatch({
            type: 'SELECT_FRUIT_BASKET',
            selectedFruitBasket: fruitBasket
        })
    }
    
    return (
        <div className="spreadsheet">
            <table>
                <thead>
                    <tr>
                    <th className="apples-header">Apples</th>
                    <th className="oranges-header">Oranges</th>
                    </tr>
                </thead>

                <tbody>
                    {fruitBasketOptions.map((fruitBasket, index) => 
                        <tr className={fruitBasket === selectedFruitBasket ? "selected-fruit-basket" : ""} onClick={() => selectFruitBasket(fruitBasket)} key={index}>
                            <td className="apples-data">{fruitBasket.nApples}</td>
                            <td className="oranges-data">{fruitBasket.nOranges}</td>
                        </tr>
                    )}
                </tbody>
            </table>
        </div>
    )
}
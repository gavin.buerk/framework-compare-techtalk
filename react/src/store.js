import { createStore } from 'redux'

function fruitBasketStore(state = {
    selectedFruitBasket: {},
    fruitBasketOptions: []
}, action) {
    switch(action.type) {
        case 'SELECT_FRUIT_BASKET':
            state.selectedFruitBasket = action.selectedFruitBasket;
            return state;
        case 'SET_FRUIT_BASKET_OPTIONS':
            state.fruitBasketOptions = action.fruitBasketOptions
            return state;
        default:
            return state
    }
}

export const store = createStore(fruitBasketStore)

async function init() {
    const response = await fetch('http://localhost:3000/api/fruit-basket-options')
    const fbos = await response.json()
    store.dispatch({
        type: 'SET_FRUIT_BASKET_OPTIONS',
        fruitBasketOptions: fbos 
    })
    const initIndex = Math.min(2, fbos.length - 1);
    store.dispatch({
        type: 'SELECT_FRUIT_BASKET',
        selectedFruitBasket: fbos[initIndex]
    })
}

init()
const express = require('express');
const cors = require('cors')
const bodyParser = require('body-parser')


const fruitBasketOptions = require('./api/fruit-basket-options');

const app = express();
app.use(cors(['*']))
app.use(bodyParser.json())

app.get('/api/fruit-basket-options', fruitBasketOptions.getAll);
app.post('/api/fruit-basket-options', fruitBasketOptions.addFruitBasket);
app.delete('/api/fruit-basket-options/:id', fruitBasketOptions.deleteFruitBasket);

const port = 3000;
app.listen(port, () => console.log(`Listening on port ${port}`));

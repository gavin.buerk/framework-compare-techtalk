const fruitbasketDao = require('../dao/FruitBasketDao.js')

exports.getAll = async (req, res) => {
  const fruitBaskets = await fruitbasketDao.getAll()
  res.status(200).send(fruitBaskets)
};

exports.addFruitBasket = async (req, res) => {
  const fruitBasket = req.body
  await fruitbasketDao.add(fruitBasket)
  res.status(201).end()
}

exports.deleteFruitBasket = async (req, res) => {
  const id = req.params.id
  await fruitbasketDao.delete(id)
  res.status(200).send({success:true})
}
const sqlite = require('sqlite')
const dbPath = './FruitBaskets.db'

const FruitbasketDao = function FruitBasketDao() {
    let db
    sqlite.open(dbPath).then(d => db = d)
    return {
        getAll() {
            return db.all(`select * from FruitBasket;`)
        },
        add(fruitBasket) {
            db.run(`insert into FruitBasket (nApples, nOranges) values (${Number(fruitBasket.nApples)}, ${Number(fruitBasket.nOranges)});`)
        },
        delete(id) {
            db.run(`delete from FruitBasket where Id = ${id};`)
        }
    }
}

module.exports = new FruitbasketDao()


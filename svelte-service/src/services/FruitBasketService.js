import { writable } from "svelte/store";

function FruitBasketService() {
    const fruitBasketOptions = writable([])
    const selectedFruitBasket = writable()

    async function init() {
        const response = await fetch('http://localhost:3000/api/fruit-basket-options')
        const fbos = await response.json()
        fruitBasketOptions.set(fbos)
        const initIndex = Math.min(2, fbos.length - 1);
        selectedFruitBasket.set(fbos[initIndex]);
    }

    init()

    return {
        selectFruitBasket(fruitBasket) {
            selectedFruitBasket.set(fruitBasket)
        },
        fruitBasketOptions: {
            subscribe: fruitBasketOptions.subscribe
        },
        selectedFruitBasket: {
            subscribe: selectedFruitBasket.subscribe
        }
    }
}

export const fruitBasketService = new FruitBasketService()
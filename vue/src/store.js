import Vuex from "vuex";
import Vue from "vue";
Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        fruitBasketOptions: [],
        selectedFruitBasket: {}
    },
    mutations: {
        selectFruitBasket(state, {selectedFruitBasket}) {
            state.selectedFruitBasket = selectedFruitBasket
        },
        setFruitBasketOptions(state, {fruitBasketOptions}) {
            state.fruitBasketOptions = fruitBasketOptions
        }
    },
    actions: {
        async fetchFruitBasketOptions({commit}) {
            const response = await fetch('http://localhost:3000/api/fruit-basket-options')
            const fbos = await response.json()
            commit({
                type: 'setFruitBasketOptions',
                fruitBasketOptions: fbos 
            })
            const initIndex = Math.min(2, fbos.length - 1);
            commit({
                type: 'selectFruitBasket',
                selectedFruitBasket: fbos[initIndex]
            })
        }
    }
});

store.dispatch('fetchFruitBasketOptions')
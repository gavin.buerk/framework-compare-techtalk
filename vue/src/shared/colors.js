export const black         = '#212121';

export const gray          = '#9e9e9e';
export const lightGray     = '#bdbdbd';
export const lighterGray   = '#e0e0e0';

export const orange        = '#ffb74d';
export const lightOrange   = '#ffcc80';
export const lighterOrange = '#ffe0b2';

export const red           = '#e57373';
export const lightRed      = '#ef9a9a';
export const lighterRed    = '#ffcdd2';

export const white         = '#ffffff';
export const offWhite      = '#fafafa';

import VueRouter from 'vue-router'
import Home from './Home'

export const router = new VueRouter({
    mode: 'history',
    routes: [
        {path: '/', redirect: '/home'},
        {path: '/home', component: Home}
    ]
})
# Comparative Web Components

This project is actually many projects in one, designed to illustrate differences between basic use cases of each framework

**Before you can run any of the apps, you must `npm install` from each app's source directory**

## How to run the apps

To simplify the process, I have made all the apps start the same way, depending on the folder you are in.  The command is:

```
npm run dev
```

**There is a caveat to this now, you need `dotnet` installed to run the dotnet server and the run command is different.** See below

**You must make sure one of the servers is started** in order for any of the apps to work.  

**After the server is started...**

You can start any or all of the apps with:

```
(cd <framework name> && npm i && npm run dev)
```

The apps will come up on predesignated ports:

| FRAMEWORK              | INSTALL & RUN CMD                                  | URL                                            |
| ---------------------  | -------------------------------------------------- | ---------------------------------------------- |
| **Server (Express)**   | `(cd server && npm i && npm run dev)`              | http://localhost:3000/api/fruit-basket-options |
| **Server (.NET Core)** | `(cd server-dotnetcore && dotnet run)`             | http://localhost:3000/api/fruit-basket-options |
| **Server (Sails)**     | `(cd server-sails && npm run dev)`                 | http://localhost:3000/api/fruit-basket-options |
| **Angular**            | `(cd angular && npm i && npm run dev)`             | http://localhost:4200                          |
| **Vue**                | `(cd vue && npm i && npm run dev)`                 | http://localhost:8080                          |
| **React**              | `(cd react && npm i && npm run dev)`               | http://localhost:3002                          |
| **LWC**                | `(cd lwc && npm i && npm run dev)`                 | http://localhost:3001                          |
| **Svelte**             | `(cd svelte && npm i && npm run dev)`              | http://localhost:5000                          |
| **None (vanilla JS)**  | `(cd vanilla-components && npm i && npm run dev)`  | http://localhost:8081                          |


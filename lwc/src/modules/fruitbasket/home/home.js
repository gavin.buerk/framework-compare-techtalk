import { LightningElement } from 'lwc';

import { selectedFruitBasketStore, fruitBasketOptionsStore } from '../store'

export default class Home extends LightningElement {
    unsubscribeSelectedFruitBasketStore;
    unsubscribeFruitBasketOptionsStore;

    connectedCallback() {
        this.unsubscribeSelectedFruitBasketStore = selectedFruitBasketStore.subscribe(selectedFruitBasket => {
            console.log(`Selected fruit basket updated: ${JSON.stringify(selectedFruitBasket, null, 2)}`);
        });
        this.unsubscribeFruitBasketOptionsStore = fruitBasketOptionsStore.subscribe(fruitBasketOptions => {
            console.log(`Fruit Basket Options updated: ${JSON.stringify(fruitBasketOptions, null, 2)}`);
        });
    }
    disconnectedCallback() {
        this.unsubscribeSelectedFruitBasketStore();
        this.unsubscribeFruitBasketOptionsStore();
    }
}

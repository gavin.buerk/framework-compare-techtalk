import {WritableStore} from './WritableStore'

function FruitBasketOptions() {
    const store = new WritableStore();
    (async function populateInitialData() {
        const response = await fetch('http://localhost:3000/api/fruit-basket-options');
        const fbos = await response.json();
        store.updateSubscribers(fbos);
        const initIndex = Math.min(2, fbos.length - 1);
        selectedFruitBasketStore.selectFruitBasket(fbos[initIndex]);
    })()
    return {
        subscribe: store.subscribe
    };
}

function SelectedFruitBasket() {
    const store = new WritableStore();
    return {
        subscribe: store.subscribe,
        selectFruitBasket(fruitBasket) {
            store.updateSubscribers(fruitBasket);
        }
    }
}

export const fruitBasketOptionsStore = new FruitBasketOptions();
export const selectedFruitBasketStore = new SelectedFruitBasket();
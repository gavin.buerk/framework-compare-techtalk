import { LightningElement, track } from 'lwc';
import { Chart } from '../chart';
import * as colors from '../colors';
import { selectedFruitBasketStore } from '../store'

export default class BarChart extends LightningElement {
    chart;
    @track selectedFruitBasket;
    unsubscribeSelectedFruitBasketStore;

    initChart() {
        this.chart = new Chart(this.template.querySelector('.piechart'), {
          type: 'bar',
          data: {
            datasets: [{
              data: [
                  this.selectedFruitBasket.nApples,
                  this.selectedFruitBasket.nOranges
              ],
              backgroundColor: [colors.red, colors.orange],
            }],
            labels: ['Apples', 'Oranges'],
          },
          options: {
            legend: {
              display: false,
            },
            scales: {
              xAxes: [{
                barPercentage: 0.75,
                gridLines: {
                  color: colors.lightGray,
                },
              }],
              yAxes: [{
                gridLines: {
                  color: colors.lightGray,
                  zeroLineColor: colors.lightGray,
                },
                ticks: {
                  beginAtZero: true,
                  callback: () => '',
                  suggestedMax: this.selectedFruitBasket.nApples + this.selectedFruitBasket.nOranges,
                  suggestedMin: 0,
                }
              }],
            },
          },
        });
    }
    
    updateChart() {
        this.chart.data.datasets[0].data = [
          this.selectedFruitBasket.nApples,
          this.selectedFruitBasket.nOranges,
        ];
    
        this.chart.update();
    }

    renderedCallback() {
        this.renderPieChart()
    }

    connectedCallback() {
        this.unsubscribeSelectedFruitBasketStore = selectedFruitBasketStore.subscribe(newVal => {
            this.selectedFruitBasket = newVal;
            this.renderPieChart();
        });
    }
    disconnectedCallback() {
        this.unsubscribeSelectedFruitBasketStore();
    }

    renderPieChart() {
        if (!this.selectedFruitBasket) return;
        if (!this.chart) {
            this.initChart()
        } else {
            this.updateChart()
        }
    }
}

import { LightningElement, track } from 'lwc';
import { Chart } from '../chart';
import * as colors from '../colors';
import { selectedFruitBasketStore } from '../store'

export default class PieChart extends LightningElement {
    chart;
    @track selectedFruitBasket;
    unsubscribeSelectedFruitBasketStore;

    renderedCallback() {
        this.renderPieChart()
    }

    connectedCallback() {
        this.unsubscribeSelectedFruitBasketStore = selectedFruitBasketStore.subscribe(newVal => {
            this.selectedFruitBasket = newVal;
            this.renderPieChart();
        });
    }
    disconnectedCallback() {
        this.unsubscribeSelectedFruitBasketStore();
    }

    initChart() {
        this.chart = new Chart(this.template.querySelector('.piechart'), {
            type: "pie",
            data: {
                datasets: [
                    {
                        data: [
                            this.selectedFruitBasket.nApples,
                            this.selectedFruitBasket.nOranges
                        ],
                        backgroundColor: [colors.red, colors.orange]
                    }
                ],
                labels: ["Apples", "Oranges"]
            }
        });
    }
    
    updateChart() {
        this.chart.data.datasets[0].data = [
            this.selectedFruitBasket.nApples,
            this.selectedFruitBasket.nOranges,
        ];
    
        this.chart.update();
    }

    renderPieChart() {
        if (!this.selectedFruitBasket) return;
        if (!this.chart) {
            this.initChart()
        } else {
            this.updateChart()
        }
    }
}

import { LightningElement, track } from 'lwc';
import { fruitBasketOptionsStore, selectedFruitBasketStore } from '../store'

export default class Spreadsheet extends LightningElement {
    @track fruitBasketOptions = [];
    @track selectedFruitBasket;

    unsubscribeFruitBasketOptionsStore;
    unsubscribeSelectedFruitBasketStore;

    connectedCallback() {
        this.unsubscribeFruitBasketOptionsStore = fruitBasketOptionsStore.subscribe(newVal => this.fruitBasketOptions = newVal);
        this.unsubscribeSelectedFruitBasketStore = selectedFruitBasketStore.subscribe(newVal => this.selectedFruitBasket = newVal);
    }
    disconnectedCallback() {
        this.unsubscribeFruitBasketOptionsStore();
        this.unsubscribeSelectedFruitBasketStore();
    }

    get internalFruitBasketOptions() {
        return this.fruitBasketOptions.map(fbo => this.addSelectedClass(fbo));
    }

    addSelectedClass(fbo) {
        fbo.selected = this.fruitBasketOptionMatchesSelected(fbo) ? 'selected-fruit-basket' : '';
        return fbo;
    }

    fruitBasketOptionMatchesSelected(fbo) {
        return this.selectedFruitBasket.nApples === fbo.nApples && this.selectedFruitBasket.nOranges === fbo.nOranges;
    }

    handleClick(e) {
        const {nApples, nOranges} = e.currentTarget.dataset;
        const fruitBasket = {nApples: Number(nApples), nOranges: Number(nOranges)};
        selectedFruitBasketStore.selectFruitBasket(fruitBasket);
    }
}

import { createElement } from 'lwc';
// import MyApp from 'my/app';
import FruitBasketApp from 'fruitbasket/app';

const app = createElement('lwc-app', { is: FruitBasketApp });
// eslint-disable-next-line @lwc/lwc/no-document-query
document.querySelector('#main').appendChild(app);

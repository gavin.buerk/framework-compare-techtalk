/**
 * Seed Function
 * (sails.config.bootstrap)
 *
 * A function that runs just before your Sails app gets lifted.
 * > Need more flexibility?  You can also create a hook.
 *
 * For more information on seeding your app with fake data, check out:
 * https://sailsjs.com/config/bootstrap
 */

module.exports.bootstrap = async function() {

  // By convention, this is a good place to set up fake data during development.
  //
  // For example:
  // ```
  // // Set up fake development data (or if we already have some, avast)
  // if (await User.count() > 0) {
  //   return;
  // }
  //
  // await User.createEach([
  //   { emailAddress: 'ry@example.com', fullName: 'Ryan Dahl', },
  //   { emailAddress: 'rachael@example.com', fullName: 'Rachael Shaw', },
  //   // etc.
  // ]);
  // ```

  // await Fruitbasket.destroy({})
  if (await Fruitbasket.count() > 0) {
    return;
  }
  await Fruitbasket.createEach([
    {nApples: 10, nOranges: 0},
    {nApples: 9, nOranges: 1},
    {nApples: 7, nOranges: 3},
    {nApples: 5, nOranges: 5},
    {nApples: 3, nOranges: 7},
    {nApples: 1, nOranges: 9},
    {nApples: 0, nOranges: 10},
  ])

};

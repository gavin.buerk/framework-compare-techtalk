"use strict";

var colors = {
    black: '#212121',
    gray: '#9e9e9e',
    lightGray: '#bdbdbd',
    lighterGray: '#e0e0e0',
    orange: '#ffb74d',
    lightOrange: '#ffcc80',
    lighterOrange: '#ffe0b2',
    red: '#e57373',
    lightRed: '#ef9a9a',
    lighterRed: '#ffcdd2',
    white: '#ffffff',
    offWhite: '#fafafa',
}

Chart.defaults.global.animation.duration = 750;
Chart.defaults.global.defaultFontColor = colors.black;
Chart.defaults.global.defaultFontFamily = `'Helvetica', sans-serif`;
Chart.defaults.global.defaultFontSize = 14;

let piechart
let barchart

function updateFruitBasketOptionsDom(fbos) {
    const spreadsheetTable = document.getElementById('spreadsheetTable')
    spreadsheetTable.innerHTML = fbos.map(fbo => `
        <tr data-n-apples="${fbo.nApples}" data-n-oranges="${fbo.nOranges}">
            <td class="apples-data">${fbo.nApples}</td>
            <td class="oranges-data">${fbo.nOranges}</td>
        </tr>
    `).join('')
    spreadsheetTable.querySelectorAll('tr').forEach(tr => tr.addEventListener('click', selectFruitBasket))
}

function selectFruitBasket(event) {
    const { nApples, nOranges } = event.currentTarget.dataset
    const fruitBasket = { nApples: Number(nApples), nOranges: Number(nOranges) }
    updateSelectedFruitBasketDom(fruitBasket)
    renderBarChart(fruitBasket)
    renderPieChart(fruitBasket)
    console.log(`Selected fruit basket updated: ${JSON.stringify(fruitBasket, null, 2)}`)
}

function updateSelectedFruitBasketDom(selectedFruitBasket) {
    document.querySelectorAll(`[data-n-apples]`).forEach(r => r.classList.remove('selected-fruit-basket'))
    const selectedFruitBasketRow = document.querySelector(`[data-n-apples='${selectedFruitBasket.nApples}'],[data-n-oranges='${selectedFruitBasket.nOranges}']`)
    selectedFruitBasketRow.classList.add('selected-fruit-basket')
}

function initPieChart(selectedFruitBasket) {
    piechart = new Chart(document.querySelector('.piechart'), {
        type: "pie",
        data: {
            datasets: [
                {
                    data: [
                        selectedFruitBasket.nApples,
                        selectedFruitBasket.nOranges
                    ],
                    backgroundColor: [colors.red, colors.orange]
                }
            ],
            labels: ["Apples", "Oranges"]
        }
    });
}

function updatePieChart(selectedFruitBasket) {
    piechart.data.datasets[0].data = [
        selectedFruitBasket.nApples,
        selectedFruitBasket.nOranges,
    ];

    piechart.update();
}

function renderPieChart(selectedFruitBasket) {
    if (!selectedFruitBasket) return
    if (!piechart) {
        initPieChart(selectedFruitBasket)
    } else {
        updatePieChart(selectedFruitBasket)
    }
}

function initChart(selectedFruitBasket) {
    barchart = new Chart(document.querySelector(".barchart"), {
        type: 'bar',
        data: {
            datasets: [{
                data: [
                    selectedFruitBasket.nApples,
                    selectedFruitBasket.nOranges
                ],
                backgroundColor: [colors.red, colors.orange],
            }],
            labels: ['Apples', 'Oranges'],
        },
        options: {
            legend: {
                display: false,
            },
            scales: {
                xAxes: [{
                    barPercentage: 0.75,
                    gridLines: {
                        color: colors.lightGray,
                    },
                }],
                yAxes: [{
                    gridLines: {
                        color: colors.lightGray,
                        zeroLineColor: colors.lightGray,
                    },
                    ticks: {
                        beginAtZero: true,
                        callback: () => '',
                        suggestedMax: selectedFruitBasket.nApples + selectedFruitBasket.nOranges,
                        suggestedMin: 0,
                    }
                }],
            },
        },
    });
}

function updateChart(selectedFruitBasket) {
    barchart.data.datasets[0].data = [
        selectedFruitBasket.nApples,
        selectedFruitBasket.nOranges,
    ];

    barchart.update();
}

function renderBarChart(selectedFruitBasket) {
    if (!selectedFruitBasket) return;
    if (!barchart) {
        initChart(selectedFruitBasket)
    } else {
        updateChart(selectedFruitBasket)
    }
}

async function populateInitialData() {
    const response = await fetch('http://localhost:3000/api/fruit-basket-options');
    const fbos = await response.json();
    updateFruitBasketOptionsDom(fbos)
    const initIndex = Math.min(2, fbos.length - 1);
    console.log(`Fruit Basket Options updated: ${JSON.stringify(fbos, null, 2)}`)
    selectFruitBasket({currentTarget:{dataset:fbos[initIndex]}})
}
populateInitialData()
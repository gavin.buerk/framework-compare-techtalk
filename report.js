const fs = require('fs')

function recurseDir(directory, cb) {
    const files = fs.readdirSync(directory, {withFileTypes: true})
    files.forEach(file => {
        if (file.isDirectory()) {
            recurseDir(`${directory}/${file.name}`, cb)
        } else {
            if (file.name.includes('.js') || 
                file.name.includes('.html') ||
                file.name.includes('.vue') ||
                file.name.includes('.svelte') ||
                file.name.includes('.jsx') ||
                file.name.includes('.ts') ||
                file.name.includes('.scss') ||
                file.name.includes('.js') 
                ) {
                // console.log(`Including ${file.name}`)
                cb(`${directory}/${file.name}`)
            } else {
                // console.log(`Excluding ${file.name}`)
            }
        }
    })

}

function recursivelySumCharacters(app) {
    let numCharacters = 0
    let numLines = 0;
    recurseDir(`${app}/src`, file => {
        const fileContents = fs.readFileSync(file, {encoding:'utf8'})
        const length = fileContents.length
        // console.log(`File ${file} contains ${length} characters`)
        numCharacters += length;
        const lines = fileContents.split('\n');
        numLines += lines.length
    })
    console.log(`${app},${numCharacters},${numLines}`)
}

['angular', 'react', 'vue', 'svelte-service'].forEach(recursivelySumCharacters)
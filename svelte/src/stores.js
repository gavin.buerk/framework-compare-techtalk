import { writable } from "svelte/store";

export const fruitBasketOptionsStore = new function FruitBasketOptionsStore() {
    const store = writable([])
    async function refresh() {
        const response = await fetch('http://localhost:3000/api/fruit-basket-options')
        const fbos = await response.json()
        store.set(fbos)
        return fbos
    }
    async function init() {
        const fbos = await refresh()
        const initIndex = Math.min(2, fbos.length - 1);
        selectedFruitBasketStore.selectFruitBasket(fbos[initIndex]);
    }
    init()
    return {
        subscribe: store.subscribe,
        refresh
    }
}

export const selectedFruitBasketStore = new function SelectedFruitBasketStore() {
    const store = writable()
    return {
        subscribe: store.subscribe,
        selectFruitBasket(fruitBasket) {
            store.set(fruitBasket)
        }
    }
}